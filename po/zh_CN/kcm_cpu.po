msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-17 00:29+0000\n"
"PO-Revision-Date: 2022-10-02 15:49\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/kinfocenter/kcm_cpu.pot\n"
"X-Crowdin-File-ID: 25615\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Tyson Tan"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "tysontan@tysontan.com"

#: main.cpp:23
#, kde-format
msgctxt "@label kcm name"
msgid "CPU"
msgstr "CPU"

#: main.cpp:24
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info:whatsthis"
msgid "Advanced CPU Information"
msgstr "高级 CPU 信息"
