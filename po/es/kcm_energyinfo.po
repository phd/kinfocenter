# Spanish translations for kcm_energyinfo.po package.
# Copyright (C) 2015 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2015.
# Eloy Cuadra <ecuadra@eloihr.net>, 2015, 2016, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kcm_energyinfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-28 00:45+0000\n"
"PO-Revision-Date: 2021-07-10 16:15+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Eloy Cuadra"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ecuadra@eloihr.net"

#: kcm.cpp:41
#, kde-format
msgid "Energy Consumption Statistics"
msgstr "Estadísticas de consumo de energía"

#: kcm.cpp:42
#, kde-format
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "This module lets you see energy information and statistics."
msgstr ""
"Este módulo le permite ver información y estadísticas sobre el consumo de "
"energía."

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Battery"
msgstr "Batería"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Rechargeable"
msgstr "Recargable"

#: package/contents/ui/main.qml:52
#, kde-format
msgid "Charge state"
msgstr "Estado de la carga"

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Current charge"
msgstr "Carga actual"

#: package/contents/ui/main.qml:53 package/contents/ui/main.qml:54
#, kde-format
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Health"
msgstr "Estado de salud"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Vendor"
msgstr "Fabricante"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Model"
msgstr "Modelo"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Serial Number"
msgstr "Número de serie"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Technology"
msgstr "Tecnología"

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Energy"
msgstr "Energía"

#: package/contents/ui/main.qml:64
#, kde-format
msgctxt "current power draw from the battery in W"
msgid "Consumption"
msgstr "Consumo"

#: package/contents/ui/main.qml:64
#, kde-format
msgctxt "Watt"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Voltage"
msgstr "Voltaje"

#: package/contents/ui/main.qml:65
#, kde-format
msgctxt "Volt"
msgid "V"
msgstr "V"

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Remaining energy"
msgstr "Energía restante"

#: package/contents/ui/main.qml:66 package/contents/ui/main.qml:67
#: package/contents/ui/main.qml:68
#, kde-format
msgctxt "Watt-hours"
msgid "Wh"
msgstr "Wh"

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Last full charge"
msgstr "Última carga completa"

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Original charge capacity"
msgstr "Capacidad de carga original"

#: package/contents/ui/main.qml:72
#, kde-format
msgid "Environment"
msgstr "Entorno"

#: package/contents/ui/main.qml:74
#, kde-format
msgid "Temperature"
msgstr "Temperatura"

#: package/contents/ui/main.qml:74
#, kde-format
msgctxt "Degree Celsius"
msgid "°C"
msgstr "°C"

#: package/contents/ui/main.qml:81
#, kde-format
msgid "Not charging"
msgstr "No está cargando"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Charging"
msgstr "Cargando"

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Discharging"
msgstr "En descarga"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Fully charged"
msgstr "Totalmente cargada"

#: package/contents/ui/main.qml:90
#, kde-format
msgid "Lithium ion"
msgstr "Ión de litio"

#: package/contents/ui/main.qml:91
#, kde-format
msgid "Lithium polymer"
msgstr "Polímero de litio"

#: package/contents/ui/main.qml:92
#, kde-format
msgid "Lithium iron phosphate"
msgstr "Litio ferrofosfato"

#: package/contents/ui/main.qml:93
#, kde-format
msgid "Lead acid"
msgstr "Plomo y ácido"

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Nickel cadmium"
msgstr "Níquel-cadmio"

#: package/contents/ui/main.qml:95
#, kde-format
msgid "Nickel metal hydride"
msgstr "Níquel-metal hidruro"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Unknown technology"
msgstr "Tecnología desconocida"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last hour"
msgstr "Última hora"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 2 hours"
msgstr "Últimas 2 horas"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 12 hours"
msgstr "Últimas 12 horas"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 24 hours"
msgstr "Últimas 24 horas"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 48 hours"
msgstr "Últimas 48 horas"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 7 days"
msgstr "Últimos 7 días"

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@info:status"
msgid "No energy information available on this system"
msgstr "Este sistema no dispone de información sobre energía"

#: package/contents/ui/main.qml:175
#, kde-format
msgid "Internal battery"
msgstr "Batería interna"

#: package/contents/ui/main.qml:176
#, kde-format
msgid "UPS battery"
msgstr "Batería del SAI"

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Monitor battery"
msgstr "Batería del monitor"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Mouse battery"
msgstr "Batería del ratón"

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Keyboard battery"
msgstr "Batería del teclado"

#: package/contents/ui/main.qml:180
#, kde-format
msgid "PDA battery"
msgstr "Batería de la PDA"

#: package/contents/ui/main.qml:181
#, kde-format
msgid "Phone battery"
msgstr "Batería del teléfono"

#: package/contents/ui/main.qml:182
#, kde-format
msgid "Unknown battery"
msgstr "Batería desconocida"

#: package/contents/ui/main.qml:200
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1% (Charging)"
msgstr "%1% (Cargando)"

#: package/contents/ui/main.qml:201
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:223
#, kde-format
msgid "Charge Percentage"
msgstr "Porcentaje de carga"

#: package/contents/ui/main.qml:233
#, kde-format
msgid "Energy Consumption"
msgstr "Consumo de energía"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Timespan"
msgstr "Intervalo de tiempo"

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Timespan of data to display"
msgstr "Intervalo de tiempo de los datos a mostrar"

#: package/contents/ui/main.qml:255
#, kde-format
msgid "Refresh"
msgstr "Refrescar"

#: package/contents/ui/main.qml:297
#, kde-format
msgctxt "Shorthand for Watts"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:297
#, kde-format
msgctxt "literal percent sign"
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:317
#, kde-format
msgid "This type of history is currently not available for this device."
msgstr ""
"Este tipo de historial no está disponible en la actualidad para este "
"dispositivo"

#: package/contents/ui/main.qml:368
#, kde-format
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/main.qml:379
#, kde-format
msgid "Yes"
msgstr "Sí"

#: package/contents/ui/main.qml:381
#, kde-format
msgid "No"
msgstr "No"

#: package/contents/ui/main.qml:399
#, kde-format
msgctxt "%1 is value, %2 is unit"
msgid "%1 %2"
msgstr "%1 %2"

#~ msgid "Application Energy Consumption"
#~ msgstr "Consumo de energía de las aplicaciones"

#~ msgid "Path: %1"
#~ msgstr "Ruta: %1"

#~ msgid "PID: %1"
#~ msgstr "PID: %1"

#~ msgid "Wakeups per second: %1 (%2%)"
#~ msgstr "Vueltas a la actividad por segundo: %1 (%2%)"

#~ msgid "Details: %1"
#~ msgstr "Detalles: %1"

#~ msgid "LeadAcid"
#~ msgstr "Plomo y ácido"

#~ msgid "Capacity degradation"
#~ msgstr "Degradación de la capacidad"

#~ msgid "Manufacturer"
#~ msgstr "Fabricante"

#~ msgid "Full design"
#~ msgstr "Diseño completo"

#~ msgid "System"
#~ msgstr "Sistema"

#~ msgid "Has power supply"
#~ msgstr "Tiene toma de corriente"

#~ msgid "Capacity"
#~ msgstr "Capacidad"
