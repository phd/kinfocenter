# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kcm_pci\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-24 00:45+0000\n"
"PO-Revision-Date: 2021-11-10 18:30+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Control Helge CardBus AGP BIST VGA Deller PME\n"
"X-POFile-SpellExtra: GiB Ternisien MAXLAT GART kcmpci Multifunções\n"
"X-POFile-SpellExtra: MINGNT PIC SERCOS FireWire WorldFip EISA mA IPMI\n"
"X-POFile-SpellExtra: CANbus ATM NuBus HyperTransport GPIB MSI APIC IRDA\n"
"X-POFile-SpellExtra: MIPS ISA ring InfiniBand Power SPP GHz PICMG\n"
"X-POFile-SpellExtra: subtractiva RF ECP SMIC MicroChannel RACEway FLASH\n"
"X-POFile-SpellExtra: RAID OHCI express Hayes Token IO EHCI DPIO FDDI BiDir\n"
"X-POFile-SpellExtra: SMBus RTC CompactPCI IPI XGA ACCESS ms SSA UHCI Co\n"
"X-POFile-IgnoreConsistency: Status\n"
"X-POFile-IgnoreConsistency: Rate\n"
"X-POFile-IgnoreConsistency: Type\n"
"X-POFile-SpellExtra: Express XHCI Infiniband NVMHCI IOMMU AHCI HPET ADMA\n"
"X-POFile-SpellExtra: SATA cards NVM smart SD Harald Sitter\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: main.cpp:26
#, kde-format
msgctxt "@label kcm name"
msgid "PCI"
msgstr "PCI"

#: main.cpp:27
#, kde-format
msgid "Harald Sitter"
msgstr "Harald Sitter"

#: package/contents/ui/main.qml:14
msgctxt "@info:whatsthis"
msgid "PCI Information"
msgstr "Informação do PCI"
