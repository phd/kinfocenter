# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: none
[General]
LogoPath=/home/me/kubuntu-circle-small.png
Website=http://www.kubuntu.org
Version=1.2.3
Variant=Unstable Branches
UseOSReleaseVersion=true # controls if os-release's VERSION is used (default=false = use VERSION_ID)
